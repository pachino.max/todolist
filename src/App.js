import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddTaskForm from './components/AddTaskForm';
import ListOfTask from './components/ListOfTask';
import uuid from 'uuid'


export default class App extends Component{
  state ={
    tasks:[{id:1, title:'Go Market'},
  {id:2, title:'Go to Shop Notebook'},
{id:3, title:'Go Walk'}],
    task: "",
    taskDone: false
  }

  handleChange =(e) =>{
    this.setState({
      task: e.target.value
    });

  }

  handleSubmit =(e) =>{
    e.preventDefault();

    const newTask = {
      title: this.state.task,
      id: this.state.id
    }
    console.log(newTask)

   
    this.setState({
      tasks: [...this.state.tasks, newTask],
      id: uuid(),
      task:'',
      taskDone:false
    });
 };
 clearList =() =>{
      this.setState({
        tasks:[]
      })
 }

 handleDelete = id =>{
   const filteredTasks = this.state.tasks.filter(task =>
    task.id !== id);
    this.setState({
      tasks: filteredTasks
    })
 }

  render(){
    return <div className='container'>
      <div className='row'>
        <div className="col-8 mx-auto">
          <h1 className='text-center'>Задачи</h1>
        <AddTaskForm task={this.state.task} 
        handleChange={this.handleChange} 
        handleSubmit={this.handleSubmit}
        />
        <ListOfTask tasks={this.state.tasks} clearList={this.clearList} handleDelete={this.handleDelete}/>
        </div>
      </div>
    </div>
    
    
   
  }

}

 
