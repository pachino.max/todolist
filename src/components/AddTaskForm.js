import React, {Component} from 'react'

export default class AddTaskForm extends Component{
    render(){
        const{tasks, handleChange,handleSubmit} = this.props
        return(
            <div className="card card-body my-4">
                <form onSubmit={handleSubmit}>
                    <div className="input-group">
                        <input type="text" 
                         className="form-control text-capitalize"
                         placeholder="добавьте задачу"
                         value={tasks}
                         onChange={handleChange}>
                        </input>
                    <button type="submit" className="btn btn-block btn-secondary mt-4">Добавить</button>
                    </div>
                </form>
            </div>
        )
    }
}