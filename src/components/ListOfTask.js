import React,{Component} from 'react'
import Task from './Task'


export default class ListOfTask extends Component{
    
   
render(){
    const {tasks, clearList,handleDelete} = this.props
   return(
       <ul className="list-group my-5">
          {tasks.map((task)=>{
              return <Task key={task.id} 
              title={task.title}
              handleDelete={()=>handleDelete(task.id)} />
          })}
           

           <button onClick={clearList} type="button" className="btn btn-block btn-primary">Очистить</button>
       </ul>
   )
}
}
