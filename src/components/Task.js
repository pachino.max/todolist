import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';



export default class Task extends Component{
    render(){
        const {title,handleDelete} = this.props
        return(
            <li className='list-group-item d-flex justify-content-between my-3'>
                <h5>{title}</h5>
                <button onClick={handleDelete}className="btn btn-danger">Удалить</button>
            </li>
        )
    }
    
        
    
    }

